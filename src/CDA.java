import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class CDA extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	private JButton browse, analyze, reset;
	private JLabel footer1, footer2;
	private JTextField csvPath;
	private JEditorPane output;
	private JScrollPane scrollPane;
	
	public CDA() throws IOException {
		setLayout(new FlowLayout());
		output = new JEditorPane("text/html", "Browse for .csv file and click \"Analyze!\"...");
		output.setText("<html><body style=\"font-family: courier;\"><center><i>Browse for .csv file and click \"Analyze!\"</i></center></html>");
		output.setEditable(false);
		scrollPane = new JScrollPane(output);
		scrollPane.setPreferredSize(new Dimension(360, 455));
		browse = new JButton("Browse...");
		csvPath = new JTextField("",28);
		csvPath.setEditable(false);
		analyze = new JButton("Analyze!");
		reset = new JButton("Reset");
		footer1 = new JLabel("Programmed by Aaron Mamparo (amampa2@uic.edu)");
		footer1.setForeground(Color.GRAY);
		footer1.setFont(new Font("Dialog", Font.PLAIN, 12));
		footer2 = new JLabel("for implementation by Kevin Thomas");
		footer2.setForeground(Color.GRAY);
		footer2.setFont(new Font("Dialog", Font.PLAIN, 12));
		add(csvPath);
		add(browse);
		add(analyze);
		add(scrollPane, BorderLayout.CENTER);
		add(reset);
		add(footer1);
		add(footer2);
		browse.addActionListener(this);
		analyze.addActionListener(this);
		reset.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e) {
		double[][] data;
		if(e.getSource() == browse){
			Chooser frame = new Chooser();
			csvPath.setText(frame.fileName);
		} else if (e.getSource() == analyze){
			if (validFileType(csvPath.getText()) == false){
				output.setText("<html><body style=\"font-family: courier;\"><font color=\"red\"><center>ERROR: That's not a valid .csv file!</center></font></html>");
			} else if(!fileExists(csvPath.getText())){
				output.setText("<html><body style=\"font-family: courier;\"><font color=\"red\"><center>ERROR: That file doesn't exist!</center></font></html>");
			} else {
				try {
					Analyzer a = new Analyzer();
					data = a.processData(csvPath.getText());
					int[] firstP2PInterval = a.findFirstPeakInterval(data);
					int[] firstT2TInterval = a.findFirstTroughInterval(data);
					int[] secondP2PInterval = a.findSecondPeakInterval(data);
					int[] secondT2TInterval = a.findSecondTroughInterval(data);
					int firstYellow = a.firstYellow(data);
					int secondYellow = a.secondYellow(data);
					output.setText("<html><body style=\"font-family: courier\"><br><center><i>" + csvPath.getText().substring(csvPath.getText().lastIndexOf('/')+1) + "</i>" +
							"<br><br>-----------<b>First Interval</b>-----------<br><br>" +
							"Dropoff Point: <i>" + firstP2PInterval[3] + "</i></center><br>" +
							"<table style=\"width:100%\"><tr><td align=\"right\" width=\"50%\">Peak to Peak:</td>" + 
							"<td align=\"left\"><font color=\"blue\">" + firstP2PInterval[0] + " - " + firstP2PInterval[1] + "</font></td></tr>" +
							"<tr><td align=\"right\">Trough to Trough:</td>" + 
							"<td align=\"left\"><font color=\"blue\">" + firstT2TInterval[0] + " - " + firstT2TInterval[1] + "</font></td></tr></table>" + 
							"<br><center>" + optimal(firstP2PInterval, firstT2TInterval) + "</center><br><br>" +
							"<center>-----------<b>Second Interval</b>-----------<br><br>" +
							"Dropoff Point: <i>" + secondP2PInterval[3] + "</i></center><br>" +
							"<table style=\"width:100%\"><tr><td align=\"right\" width=\"50%\">Peak to Peak:</td>" + 
							"<td align=\"left\"><font color=\"green\">" + secondP2PInterval[0] + " - " + secondP2PInterval[1] + "</font></td></tr>" +
							"<tr><td align=\"right\">Trough to Trough:</td>" + 
							"<td align=\"left\"><font color=\"green\">" + secondT2TInterval[0] + " - " + secondT2TInterval[1] + "</font></td></tr></table>" +
							"<br><center>" + optimal(secondP2PInterval, secondT2TInterval) + "</center><br><br>" +
							"<center>------------<b>Yellow Values</b>------------</center>" +
							"<table style=\"width:100%\"><tr><td align=\"right\" width=\"50%\">First:</td>" + 
							"<td align=\"left\">" + firstYellow + "</td></tr>" +
							"<tr><td align=\"right\">Second:</td>" + 
							"<td align=\"left\">" + secondYellow + "</td></tr></table><br></html>");
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (@SuppressWarnings("hiding") IOException e1) {
					e1.printStackTrace();
				}
			}
		} else if(e.getSource() == reset){
			output.setText("<html><body style=\"font-family: courier;\"><center>Browse for .csv file and click \"Analyze!\"</center></html>");
			csvPath.setText("");
		}
	}
	
	public String optimal(int[] peak, int[] trough){
		if(peak[2] < trough[2]){
			return "<b>Peak</b> is optimal.";
		} else if(peak[2] > trough[2]){
			return "<b>Trough</b> is optimal.";
		} else if(peak[2] == trough[2]){
			return "<b>Both</b> are optimal.";
		} else return null;
	}
	
	public boolean validFileType(String path){
		if(path.endsWith(".csv")){
			return true;
		} else return false;
	}
	
	public boolean fileExists(String path){
		File f = new File(path);
		if(f.exists()){
			return true;
		} else return false;
	}

	class Chooser extends JFrame {
		private static final long serialVersionUID = -9019943509428869930L;
		JFileChooser chooser;
		String fileName;
		public Chooser() {
			chooser = new JFileChooser();
			int r = chooser.showOpenDialog(new JFrame());
			if (r == JFileChooser.APPROVE_OPTION) {
				fileName = chooser.getSelectedFile().getPath();
			}
		}
	}
	
	public static void main (String args[]) throws IOException {
		CDA gui = new CDA();
		gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gui.setSize(375, 630);
		gui.setResizable(false);
		gui.setVisible(true);
		gui.setTitle("CSV Data Analyzer");
	}
}
