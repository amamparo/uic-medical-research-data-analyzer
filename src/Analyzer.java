import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;


public class Analyzer {
	public double[][] processData(String path) throws FileNotFoundException{
		try {
			int rowCount = countRows(path);
			double[][] data = new double[rowCount-1][4];
			Scanner csvFile = new Scanner(new FileReader(path));
			int i=0;
			while(i<rowCount-1){
				String line = csvFile.nextLine();
				if(line.startsWith(",")){
					break;
				}
				StringTokenizer st = new StringTokenizer(line, ",", false);
				for(int j=0; j<4; j++){
					if(j==3 && i<11){
						data[i][j] = 0;
					} else {
						data[i][j] = Double.parseDouble(st.nextToken());
					}
				}
				i++;
			}
			return data;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	final double thresh = .2;
	
	public int[] findFirstPeakInterval(double[][] data){
		//begin data-collection at 2000 seconds
		//find location of BEGINNING of noticeable drop in pink line (first column of data[][])
		//go 30 rows back from ^that x location & store location of nearest peak on yellow line (third column of data[][])
		//go 120 rows back from ^that x location & store location of nearest peak on same yellow line
		int[] interval = new int [4];	//this will be returned... first value is beginning point & second value is ending point
		int x1=0;
		int x=0; //this will be the dropoff point-1
		for(int i = 2000; i< Integer.MAX_VALUE; i++){
			if(Double.valueOf(data[i-10][2] - data[i][2]) > thresh){
				x1=i;
				break;
			}
		}
		for(int k = 1; k<=10; k++){
			if(data[x1-k][2] < data[x1-k+1][2]){
				x = x1-k+1;
				interval[3] = x+1;
				break;
			}
		}
		//now analyze the yellow line (data[...][2]) based on x
		int end = x-30;	//count 30 secs back from pink's dropoff point
		
		//first check whether or not it's a peak, and then whether or not it's a minimum
		if ((data[end-1][2] < data[end][2]) && (data[end+1][2] < data[end][2])){
			//is a peak, do nothing
		} else if ((data[end-1][2] > data[end][2]) && (data[end+1][2] > data[end][2])) {
			//is a trough. check both sides for closer peak
			int forwardDistance = 0;
			int backwardDistance = 0;
			for(int i = 1; i<Integer.MAX_VALUE; i++){
				//check forward peak
				if(data[end+i][2] < data[end+i-1][2]){
					forwardDistance = i-1;
					break;
				}
			}
			for(int i = 1; i<Integer.MAX_VALUE; i++){
				//check backwards peak
				if(data[end-i][2] < data[end-i+1][2]){
					backwardDistance = i+1;
					break;
				}
			}
			
			if(backwardDistance < forwardDistance){
				end = end-backwardDistance;
				interval[2] = backwardDistance;
			} else if (backwardDistance > forwardDistance){
				end = end+forwardDistance;
				interval[2] = forwardDistance;
			} else if (backwardDistance == forwardDistance){
				end = end + forwardDistance;
				interval[2] = forwardDistance;
			}
		} else {
			//find nearest peak by traversing forward & backwards to two different peaks and finding nearest
			//first check slope at point
			interval[2] = 0;
			int forwardDistance = 0;
			int backwardDistance = 0;
			if(data[end+1][2] > data[end-1][2]){
				//positive slope at interval endpoint... check peak at either side and choose the closer one
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check forward peak
					if(data[end+i][2] < data[end+i-1][2]){
						forwardDistance = i-1;
						break;
					}
				}
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check backwards peak. harder... skip one trough before peak
					if(data[end-i][2] > data[end-i+1][2]){
						//trough found... continue further until peak reached
						for(int j = 0; j<Integer.MAX_VALUE; j++){
							if(data[end-i-j][2] < data[end-i-j+1][2]){
								backwardDistance = i-j+1;
								break;
							}
						}
						break;
					}
				}
				
				if(backwardDistance < forwardDistance){
					end = end-backwardDistance;
					interval[2] = backwardDistance;
				} else if (backwardDistance > forwardDistance){
					end = end+forwardDistance;
					interval[2] = forwardDistance;
				} else if (backwardDistance == forwardDistance){
					end = end + forwardDistance;
					interval[2] = forwardDistance;
				}
					
			} else if(data[end+1][2] < data[end-1][2]){
				//negative slope at interval endpoint... find peak at both sides & pick the closer one
				interval[2] = 0;
				forwardDistance = 0;
				backwardDistance = 0;
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check backwards peak
					if(data[end-i][2] < data[end-i+1][2]){
						backwardDistance = i+1;
						break;
					}
				}
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check forward peak. harder... skip one trough before peak
					if(data[end+i][2] < data[end+i+1][2]){
						//trough found... continue further until peak reached
						for(int j = 0; j<Integer.MAX_VALUE; j++){
							if(data[end+i+j][2] > data[end+i+j+1][2]){
								forwardDistance = i+j;
								break;
							}
						}
						break;
					}
				}
				if(backwardDistance < forwardDistance){
					end = end-backwardDistance;
					interval[2] = backwardDistance;
				} else if (backwardDistance > forwardDistance){
					end = end+forwardDistance;
					interval[2] = forwardDistance;
				} else if (backwardDistance == forwardDistance){
					end = end+forwardDistance;
					interval[2] = forwardDistance;
				}
			}	
		}
		//by now, the peak2peak end point for the first interval has been found...
		//now go back 120 sec & find nearest peak... that will be beginning of interval
		int begin = end - 120;
		if ((data[begin-1][2] < data[begin][2]) && (data[begin+1][2] < data[begin][2])){
			//is a peak, do nothing
		} else if ((data[begin-1][2] > data[begin][2]) && (data[begin+1][2] > data[begin][2])) {
			//is a trough. check both sides for closer peak
			int forwardDistance = 0;
			int backwardDistance = 0;
			for(int i = 1; i<Integer.MAX_VALUE; i++){
				//check forward peak
				if(data[begin+i][2] < data[begin+i-1][2]){
					forwardDistance = i-1;
					break;
				}
			}
			for(int i = 1; i<Integer.MAX_VALUE; i++){
				//check backwards peak
				if(data[begin-i][2] < data[begin-i+1][2]){
					backwardDistance = i+1;
					break;
				}
			}
			
			if(backwardDistance < forwardDistance){
				begin = begin-backwardDistance;
			} else if (backwardDistance > forwardDistance){
				begin = begin+forwardDistance;
			} else if (backwardDistance == forwardDistance){
				if(x-end < 30){
					begin = begin + forwardDistance;
				}else if(x-end > 30){
					begin = begin-backwardDistance;
				}
			}
		} else {
			//find nearest peak by traversing forward & backwards to two different peaks and finding nearest
			//first check slope at point
			int forwardDistance = 0;
			int backwardDistance = 0;
			if(data[begin+1][2] > data[begin-1][2]){
				//positive slope at interval endpoint... check peak at either side and choose the closer one
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check forward peak
					if(data[begin+i][2] < data[begin+i-1][2]){
						forwardDistance = i-1;
						break;
					}
				}
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check backwards peak. harder... skip one trough before peak
					if(data[begin-i][2] > data[begin-i+1][2]){
						//trough found... continue further until peak reached
						for(int j = 0; j<Integer.MAX_VALUE; j++){
							if(data[begin-i-j][2] < data[begin-i-j+1][2]){
								backwardDistance = i-j+1;
								break;
							}
						}
						break;
					}
				}
				
				if(backwardDistance < forwardDistance){
					begin = begin-backwardDistance;
				} else if (backwardDistance > forwardDistance){
					begin = begin+forwardDistance;
				} else if (backwardDistance == forwardDistance){
					begin = begin+forwardDistance;
				}
					
			} else if(data[begin+1][2] < data[begin-1][2]){
				//negative slope at interval endpoint... find peak at both sides & pick the closer one
				forwardDistance = 0;
				backwardDistance = 0;
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check backwards peak
					if(data[begin-i][2] < data[begin-i+1][2]){
						backwardDistance = i+1;
						break;
					}
				}
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check forward peak. harder... skip one trough before peak
					if(data[begin+i][2] < data[begin+i+1][2]){
						//trough found... continue further until peak reached
						for(int j = 0; j<Integer.MAX_VALUE; j++){
							if(data[begin+i+j][2] > data[begin+i+j+1][2]){
								forwardDistance = i+j;
								break;
							}
						}
						break;
					}
				}
				if(backwardDistance < forwardDistance){
					begin = begin-backwardDistance;
				} else if (backwardDistance > forwardDistance){
					begin = begin+forwardDistance;
				} else if (backwardDistance == forwardDistance){
					if(x-end < 30){
						begin = begin + forwardDistance;
					}else if(x-end > 30){
						begin = begin-backwardDistance;
					}
				}
			}	
		}
		interval[0] = begin+1;
		interval[1] = end+1;
		return interval;
	}
	
	
	public int[] findFirstTroughInterval(double[][] data){
		//begin data-collection at 2000 seconds
		//find location of BEGINNING of noticeable drop in pink line (first column of data[][])
		//go 30 rows back from ^that x location & store location of nearest trough on yellow line (third column of data[][])
		//go 120 rows back from ^that x location & store location of nearest trough on same yellow line
		int[] interval = new int [4];	//this will be returned... first value is beginning point & second value is ending point
		int x1=0;
		int x=0; //this will be the dropoff point-1
		for(int i = 2000; i< Integer.MAX_VALUE; i++){
			if(Double.valueOf(data[i-10][2] - data[i][2]) > thresh){
				x1=i;
				break;
			}
		}
		for(int k = 1; k<=10; k++){
			if(data[x1-k][2] < data[x1-k+1][2]){
				x = x1-k+1;
				interval[3] = x+1;
				break;
			}
		}
		//now analyze the yellow line (data[...][2]) based on x
		int end = x-30;	//count 30 secs back from pink's dropoff point
		
		//first check whether or not it's a trough, and then whether or not it's a peak
		if ((data[end-1][2] > data[end][2]) && (data[end+1][2] > data[end][2])){
			//is a trough, do nothing
		} else if ((data[end-1][2] < data[end][2]) && (data[end+1][2] > data[end][2])) {
			//is a peak. check both sides for closer trough
			int forwardDistance = 0;
			int backwardDistance = 0;
			for(int i = 1; i<Integer.MAX_VALUE; i++){
				//check forward peak
				if(data[end+i][2] > data[end+i-1][2]){
					forwardDistance = i-1;
					break;
				}
			}
			for(int i = 1; i<Integer.MAX_VALUE; i++){
				//check backwards peak
				if(data[end-i][2] > data[end-i+1][2]){
					backwardDistance = i+1;
					break;
				}
			}
			
			if(backwardDistance < forwardDistance){
				end = end-backwardDistance;
				interval[2] = backwardDistance;
			} else if (backwardDistance > forwardDistance){
				end = end+forwardDistance;
				interval[2] = forwardDistance;
			} else if (backwardDistance == forwardDistance){
				end = end + forwardDistance;
				interval[2] = forwardDistance;
			}
		} else {
			//find nearest peak by traversing forward & backwards to two different troughs and finding nearest
			//first check slope at point
			interval[2] = 0;
			int forwardDistance = 0;
			int backwardDistance = 0;
			if(data[end+1][2] < data[end-1][2]){
				//negative slope at interval endpoint... check trough at either side and choose the closer one
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check forward trough
					if(data[end+i][2] > data[end+i-1][2]){
						forwardDistance = i-1;
						break;
					}
				}
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check backwards trough. harder... skip one peak before trough
					if(data[end-i][2] < data[end-i+1][2]){
						//peak found... continue further until trough reached
						for(int j = 0; j<Integer.MAX_VALUE; j++){
							if(data[end-i-j][2] > data[end-i-j+1][2]){
								backwardDistance = i-j+1;
								break;
							}
						}
						break;
					}
				}
				
				if(backwardDistance < forwardDistance){
					end = end-backwardDistance;
					interval[2] = backwardDistance;
				} else if (backwardDistance > forwardDistance){
					end = end+forwardDistance;
					interval[2] = forwardDistance;
				} else if (backwardDistance == forwardDistance){
					end = end + forwardDistance;
					interval[2] = forwardDistance;
				}
					
			} else if(data[end+1][2] > data[end-1][2]){
				//positive slope at interval endpoint... find trough at both sides & pick the closer one
				interval[2] = 0;
				forwardDistance = 0;
				backwardDistance = 0;
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check backwards trough
					if(data[end-i][2] > data[end-i+1][2]){
						backwardDistance = i+1;
						break;
					}
				}
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check forward trough. harder... skip one peak before trough
					if(data[end+i][2] > data[end+i+1][2]){
						//peak found... continue further until trough reached
						for(int j = 0; j<Integer.MAX_VALUE; j++){
							if(data[end+i+j][2] < data[end+i+j+1][2]){
								forwardDistance = i+j;
								break;
							}
						}
						break;
					}
				}
				if(backwardDistance < forwardDistance){
					end = end-backwardDistance;
					interval[2] = backwardDistance;
				} else if (backwardDistance > forwardDistance){
					end = end+forwardDistance;
					interval[2] = forwardDistance;
				} else if (backwardDistance == forwardDistance){
					end = end+forwardDistance;
					interval[2] = forwardDistance;
				}
			}	
		}
		//by now, the trough2trough end point for the first interval has been found...
		//now go back 120 sec & find nearest trough... that will be beginning of interval
		int begin = end - 120;
		if ((data[begin-1][2] > data[begin][2]) && (data[begin+1][2] > data[begin][2])){
			//is a trough, do nothing
		} else if ((data[begin-1][2] < data[begin][2]) && (data[begin+1][2] < data[begin][2])) {
			//is a peak. check both sides for closer trough
			int forwardDistance = 0;
			int backwardDistance = 0;
			for(int i = 1; i<Integer.MAX_VALUE; i++){
				//check forward trough
				if(data[begin+i][2] > data[begin+i-1][2]){
					forwardDistance = i-1;
					break;
				}
			}
			for(int i = 1; i<Integer.MAX_VALUE; i++){
				//check backwards trough
				if(data[begin-i][2] > data[begin-i+1][2]){
					backwardDistance = i+1;
					break;
				}
			}
			
			if(backwardDistance < forwardDistance){
				begin = begin-backwardDistance;
			} else if (backwardDistance > forwardDistance){
				begin = begin+forwardDistance;
			} else if (backwardDistance == forwardDistance){
				if(x-end < 30){
					begin = begin + forwardDistance;
				}else if(x-end > 30){
					begin = begin-backwardDistance;
				}
			}
		} else {
			//find nearest peak by traversing forward & backwards to two different peaks and finding nearest
			//first check slope at point
			int forwardDistance = 0;
			int backwardDistance = 0;
			if(data[begin+1][2] < data[begin-1][2]){
				//negative slope at interval endpoint... check trourgh at either side and choose the closer one
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check forward trough
					if(data[begin+i][2] > data[begin+i-1][2]){
						forwardDistance = i-1;
						break;
					}
				}
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check backwards trough. harder... skip one peak before trough
					if(data[begin-i][2] < data[begin-i+1][2]){
						//peak found... continue further until trough reached
						for(int j = 0; j<Integer.MAX_VALUE; j++){
							if(data[begin-i-j][2] > data[begin-i-j+1][2]){
								backwardDistance = i-j+1;
								break;
							}
						}
						break;
					}
				}
				
				if(backwardDistance < forwardDistance){
					begin = begin-backwardDistance;
				} else if (backwardDistance > forwardDistance){
					begin = begin+forwardDistance;
				} else if (backwardDistance == forwardDistance){
					begin = begin+forwardDistance;
				}
					
			} else if(data[begin+1][2] < data[begin-1][2]){
				//positive slope at interval endpoint... find trough at both sides & pick the closer one
				forwardDistance = 0;
				backwardDistance = 0;
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check backwards trough
					if(data[begin-i][2] > data[begin-i+1][2]){
						backwardDistance = i+1;
						break;
					}
				}
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check forward trough. harder... skip one peak before trough
					if(data[begin+i][2] > data[begin+i+1][2]){
						//peak found... continue further until trough reached
						for(int j = 0; j<Integer.MAX_VALUE; j++){
							if(data[begin+i+j][2] < data[begin+i+j+1][2]){
								forwardDistance = i+j;
								break;
							}
						}
						break;
					}
				}
				if(backwardDistance < forwardDistance){
					begin = begin-backwardDistance;
				} else if (backwardDistance > forwardDistance){
					begin = begin+forwardDistance;
				} else if (backwardDistance == forwardDistance){
					if(x-end < 30){
						begin = begin + forwardDistance;
					}else if(x-end > 30){
						begin = begin-backwardDistance;
					}
				}
			}	
		}
		interval[0] = begin+1;
		interval[1] = end+1;
		return interval;
	}
	
	
	public int[] findSecondPeakInterval(double[][] data){
		//begin data-collection at 2000 seconds
		//find location of BEGINNING of noticeable drop in pink line (first column of data[][])
		//go 30 rows back from ^that x location & store location of nearest peak on yellow line (third column of data[][])
		//go 120 rows back from ^that x location & store location of nearest peak on same yellow line
		int[] interval = new int [4];	//this will be returned... first value is beginning point & second value is ending point
		int x1=0;
		int x=0; //this will be the dropoff point-1
		for(int i = 4000; i< Integer.MAX_VALUE; i++){
			if(Double.valueOf(data[i-10][2] - data[i][2]) > thresh){
				x1=i;
				break;
			}
		}
		for(int k = 1; k<=10; k++){
			if(data[x1-k][2] < data[x1-k+1][2]){
				x = x1-k+1;
				interval[3] = x+1;
				break;
			}
		}
		//now analyze the yellow line (data[...][2]) based on x
		int end = x-30;	//count 30 secs back from pink's dropoff point
		
		//first check whether or not it's a peak, and then whether or not it's a minimum
		if ((data[end-1][2] < data[end][2]) && (data[end+1][2] < data[end][2])){
			//is a peak, do nothing
		} else if ((data[end-1][2] > data[end][2]) && (data[end+1][2] > data[end][2])) {
			//is a trough. check both sides for closer peak
			int forwardDistance = 0;
			int backwardDistance = 0;
			for(int i = 1; i<Integer.MAX_VALUE; i++){
				//check forward peak
				if(data[end+i][2] < data[end+i-1][2]){
					forwardDistance = i-1;
					break;
				}
			}
			for(int i = 1; i<Integer.MAX_VALUE; i++){
				//check backwards peak
				if(data[end-i][2] < data[end-i+1][2]){
					backwardDistance = i+1;
					break;
				}
			}
			
			if(backwardDistance < forwardDistance){
				end = end-backwardDistance;
				interval[2] = backwardDistance;
			} else if (backwardDistance > forwardDistance){
				end = end+forwardDistance;
				interval[2] = forwardDistance;
			} else if (backwardDistance == forwardDistance){
				end = end + forwardDistance;
				interval[2] = forwardDistance;
			}
		} else {
			//find nearest peak by traversing forward & backwards to two different peaks and finding nearest
			//first check slope at point
			interval[2] = 0;
			int forwardDistance = 0;
			int backwardDistance = 0;
			if(data[end+1][2] > data[end-1][2]){
				//positive slope at interval endpoint... check peak at either side and choose the closer one
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check forward peak
					if(data[end+i][2] < data[end+i-1][2]){
						forwardDistance = i-1;
						break;
					}
				}
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check backwards peak. harder... skip one trough before peak
					if(data[end-i][2] > data[end-i+1][2]){
						//trough found... continue further until peak reached
						for(int j = 0; j<Integer.MAX_VALUE; j++){
							if(data[end-i-j][2] < data[end-i-j+1][2]){
								backwardDistance = i-j+1;
								break;
							}
						}
						break;
					}
				}
				
				if(backwardDistance < forwardDistance){
					end = end-backwardDistance;
					interval[2] = backwardDistance;
				} else if (backwardDistance > forwardDistance){
					end = end+forwardDistance;
					interval[2] = forwardDistance;
				} else if (backwardDistance == forwardDistance){
					end = end + forwardDistance;
					interval[2] = forwardDistance;
				}
					
			} else if(data[end+1][2] < data[end-1][2]){
				//negative slope at interval endpoint... find peak at both sides & pick the closer one
				interval[2] = 0;
				forwardDistance = 0;
				backwardDistance = 0;
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check backwards peak
					if(data[end-i][2] < data[end-i+1][2]){
						backwardDistance = i+1;
						break;
					}
				}
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check forward peak. harder... skip one trough before peak
					if(data[end+i][2] < data[end+i+1][2]){
						//trough found... continue further until peak reached
						for(int j = 0; j<Integer.MAX_VALUE; j++){
							if(data[end+i+j][2] > data[end+i+j+1][2]){
								forwardDistance = i+j;
								break;
							}
						}
						break;
					}
				}
				if(backwardDistance < forwardDistance){
					end = end-backwardDistance;
					interval[2] = backwardDistance;
				} else if (backwardDistance > forwardDistance){
					end = end+forwardDistance;
					interval[2] = forwardDistance;
				} else if (backwardDistance == forwardDistance){
					end = end+forwardDistance;
					interval[2] = forwardDistance;
				}
			}	
		}
		//by now, the peak2peak end point for the first interval has been found...
		//now go back 120 sec & find nearest peak... that will be beginning of interval
		int begin = end - 120;
		if ((data[begin-1][2] < data[begin][2]) && (data[begin+1][2] < data[begin][2])){
			//is a peak, do nothing
		} else if ((data[begin-1][2] > data[begin][2]) && (data[begin+1][2] > data[begin][2])) {
			//is a trough. check both sides for closer peak
			int forwardDistance = 0;
			int backwardDistance = 0;
			for(int i = 1; i<Integer.MAX_VALUE; i++){
				//check forward peak
				if(data[begin+i][2] < data[begin+i-1][2]){
					forwardDistance = i-1;
					break;
				}
			}
			for(int i = 1; i<Integer.MAX_VALUE; i++){
				//check backwards peak
				if(data[begin-i][2] < data[begin-i+1][2]){
					backwardDistance = i+1;
					break;
				}
			}
			
			if(backwardDistance < forwardDistance){
				begin = begin-backwardDistance;
			} else if (backwardDistance > forwardDistance){
				begin = begin+forwardDistance;
			} else if (backwardDistance == forwardDistance){
				if(x-end < 30){
					begin = begin + forwardDistance;
				}else if(x-end > 30){
					begin = begin-backwardDistance;
				}
			}
		} else {
			//find nearest peak by traversing forward & backwards to two different peaks and finding nearest
			//first check slope at point
			int forwardDistance = 0;
			int backwardDistance = 0;
			if(data[begin+1][2] > data[begin-1][2]){
				//positive slope at interval endpoint... check peak at either side and choose the closer one
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check forward peak
					if(data[begin+i][2] < data[begin+i-1][2]){
						forwardDistance = i-1;
						break;
					}
				}
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check backwards peak. harder... skip one trough before peak
					if(data[begin-i][2] > data[begin-i+1][2]){
						//trough found... continue further until peak reached
						for(int j = 0; j<Integer.MAX_VALUE; j++){
							if(data[begin-i-j][2] < data[begin-i-j+1][2]){
								backwardDistance = i-j+1;
								break;
							}
						}
						break;
					}
				}
				
				if(backwardDistance < forwardDistance){
					begin = begin-backwardDistance;
				} else if (backwardDistance > forwardDistance){
					begin = begin+forwardDistance;
				} else if (backwardDistance == forwardDistance){
					begin = begin+forwardDistance;
				}
					
			} else if(data[begin+1][2] < data[begin-1][2]){
				//negative slope at interval endpoint... find peak at both sides & pick the closer one
				forwardDistance = 0;
				backwardDistance = 0;
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check backwards peak
					if(data[begin-i][2] < data[begin-i+1][2]){
						backwardDistance = i+1;
						break;
					}
				}
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check forward peak. harder... skip one trough before peak
					if(data[begin+i][2] < data[begin+i+1][2]){
						//trough found... continue further until peak reached
						for(int j = 0; j<Integer.MAX_VALUE; j++){
							if(data[begin+i+j][2] > data[begin+i+j+1][2]){
								forwardDistance = i+j;
								break;
							}
						}
						break;
					}
				}
				if(backwardDistance < forwardDistance){
					begin = begin-backwardDistance;
				} else if (backwardDistance > forwardDistance){
					begin = begin+forwardDistance;
				} else if (backwardDistance == forwardDistance){
					if(x-end < 30){
						begin = begin + forwardDistance;
					}else if(x-end > 30){
						begin = begin-backwardDistance;
					}
				}
			}	
		}
		interval[0] = begin+1;
		interval[1] = end+1;
		return interval;
	}
	
	
	public int[] findSecondTroughInterval(double[][] data){
		//begin data-collection at 2000 seconds
		//find location of BEGINNING of noticeable drop in pink line (first column of data[][])
		//go 30 rows back from ^that x location & store location of nearest trough on yellow line (third column of data[][])
		//go 120 rows back from ^that x location & store location of nearest trough on same yellow line
		int[] interval = new int [4];	//this will be returned... first value is beginning point & second value is ending point
		int x1=0;
		int x=0; //this will be the dropoff point-1
		for(int i = 4000; i< Integer.MAX_VALUE; i++){
			if(Double.valueOf(data[i-10][2] - data[i][2]) > thresh){
				x1=i;
				break;
			}
		}
		for(int k = 1; k<=10; k++){
			if(data[x1-k][2] < data[x1-k+1][2]){
				x = x1-k+1;
				interval[3] = x+1;
				break;
			}
		}
		//now analyze the yellow line (data[...][2]) based on x
		int end = x-30;	//count 30 secs back from pink's dropoff point
		
		//first check whether or not it's a trough, and then whether or not it's a peak
		if ((data[end-1][2] > data[end][2]) && (data[end+1][2] > data[end][2])){
			//is a trough, do nothing
		} else if ((data[end-1][2] < data[end][2]) && (data[end+1][2] > data[end][2])) {
			//is a peak. check both sides for closer trough
			int forwardDistance = 0;
			int backwardDistance = 0;
			for(int i = 1; i<Integer.MAX_VALUE; i++){
				//check forward peak
				if(data[end+i][2] > data[end+i-1][2]){
					forwardDistance = i-1;
					break;
				}
			}
			for(int i = 1; i<Integer.MAX_VALUE; i++){
				//check backwards peak
				if(data[end-i][2] > data[end-i+1][2]){
					backwardDistance = i+1;
					break;
				}
			}
			
			if(backwardDistance < forwardDistance){
				end = end-backwardDistance;
				interval[2] = backwardDistance;
			} else if (backwardDistance > forwardDistance){
				end = end+forwardDistance;
				interval[2] = forwardDistance;
			} else if (backwardDistance == forwardDistance){
				end = end + forwardDistance;
				interval[2] = forwardDistance;
			}
		} else {
			//find nearest peak by traversing forward & backwards to two different troughs and finding nearest
			//first check slope at point
			interval[2] = 0;
			int forwardDistance = 0;
			int backwardDistance = 0;
			if(data[end+1][2] < data[end-1][2]){
				//negative slope at interval endpoint... check trough at either side and choose the closer one
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check forward trough
					if(data[end+i][2] > data[end+i-1][2]){
						forwardDistance = i-1;
						break;
					}
				}
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check backwards trough. harder... skip one peak before trough
					if(data[end-i][2] < data[end-i+1][2]){
						//peak found... continue further until trough reached
						for(int j = 0; j<Integer.MAX_VALUE; j++){
							if(data[end-i-j][2] > data[end-i-j+1][2]){
								backwardDistance = i-j+1;
								break;
							}
						}
						break;
					}
				}
				
				if(backwardDistance < forwardDistance){
					end = end-backwardDistance;
					interval[2] = backwardDistance;
				} else if (backwardDistance > forwardDistance){
					end = end+forwardDistance;
					interval[2] = forwardDistance;
				} else if (backwardDistance == forwardDistance){
					end = end + forwardDistance;
					interval[2] = forwardDistance;
				}
					
			} else if(data[end+1][2] > data[end-1][2]){
				//positive slope at interval endpoint... find trough at both sides & pick the closer one
				interval[2] = 0;
				forwardDistance = 0;
				backwardDistance = 0;
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check backwards trough
					if(data[end-i][2] > data[end-i+1][2]){
						backwardDistance = i+1;
						break;
					}
				}
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check forward trough. harder... skip one peak before trough
					if(data[end+i][2] > data[end+i+1][2]){
						//peak found... continue further until trough reached
						for(int j = 0; j<Integer.MAX_VALUE; j++){
							if(data[end+i+j][2] < data[end+i+j+1][2]){
								forwardDistance = i+j;
								break;
							}
						}
						break;
					}
				}
				if(backwardDistance < forwardDistance){
					end = end-backwardDistance;
					interval[2] = backwardDistance;
				} else if (backwardDistance > forwardDistance){
					end = end+forwardDistance;
					interval[2] = forwardDistance;
				} else if (backwardDistance == forwardDistance){
					end = end+forwardDistance;
					interval[2] = forwardDistance;
				}
			}	
		}
		//by now, the trough2trough end point for the first interval has been found...
		//now go back 120 sec & find nearest trough... that will be beginning of interval
		int begin = end - 120;
		if ((data[begin-1][2] > data[begin][2]) && (data[begin+1][2] > data[begin][2])){
			//is a trough, do nothing
		} else if ((data[begin-1][2] < data[begin][2]) && (data[begin+1][2] < data[begin][2])) {
			//is a peak. check both sides for closer trough
			int forwardDistance = 0;
			int backwardDistance = 0;
			for(int i = 1; i<Integer.MAX_VALUE; i++){
				//check forward trough
				if(data[begin+i][2] > data[begin+i-1][2]){
					forwardDistance = i-1;
					break;
				}
			}
			for(int i = 1; i<Integer.MAX_VALUE; i++){
				//check backwards trough
				if(data[begin-i][2] > data[begin-i+1][2]){
					backwardDistance = i+1;
					break;
				}
			}
			
			if(backwardDistance < forwardDistance){
				begin = begin-backwardDistance;
			} else if (backwardDistance > forwardDistance){
				begin = begin+forwardDistance;
			} else if (backwardDistance == forwardDistance){
				if(x-end < 30){
					begin = begin + forwardDistance;
				}else if(x-end > 30){
					begin = begin-backwardDistance;
				}
			}
		} else {
			//find nearest peak by traversing forward & backwards to two different peaks and finding nearest
			//first check slope at point
			int forwardDistance = 0;
			int backwardDistance = 0;
			if(data[begin+1][2] < data[begin-1][2]){
				//negative slope at interval endpoint... check trourgh at either side and choose the closer one
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check forward trough
					if(data[begin+i][2] > data[begin+i-1][2]){
						forwardDistance = i-1;
						break;
					}
				}
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check backwards trough. harder... skip one peak before trough
					if(data[begin-i][2] < data[begin-i+1][2]){
						//peak found... continue further until trough reached
						for(int j = 0; j<Integer.MAX_VALUE; j++){
							if(data[begin-i-j][2] > data[begin-i-j+1][2]){
								backwardDistance = i-j+1;
								break;
							}
						}
						break;
					}
				}
				
				if(backwardDistance < forwardDistance){
					begin = begin-backwardDistance;
				} else if (backwardDistance > forwardDistance){
					begin = begin+forwardDistance;
				} else if (backwardDistance == forwardDistance){
					begin = begin+forwardDistance;
				}
					
			} else if(data[begin+1][2] < data[begin-1][2]){
				//positive slope at interval endpoint... find trough at both sides & pick the closer one
				forwardDistance = 0;
				backwardDistance = 0;
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check backwards trough
					if(data[begin-i][2] > data[begin-i+1][2]){
						backwardDistance = i+1;
						break;
					}
				}
				for(int i = 1; i<Integer.MAX_VALUE; i++){
					//check forward trough. harder... skip one peak before trough
					if(data[begin+i][2] > data[begin+i+1][2]){
						//peak found... continue further until trough reached
						for(int j = 0; j<Integer.MAX_VALUE; j++){
							if(data[begin+i+j][2] < data[begin+i+j+1][2]){
								forwardDistance = i+j;
								break;
							}
						}
						break;
					}
				}
				if(backwardDistance < forwardDistance){
					begin = begin-backwardDistance;
				} else if (backwardDistance > forwardDistance){
					begin = begin+forwardDistance;
				} else if (backwardDistance == forwardDistance){
					if(x-end < 30){
						begin = begin + forwardDistance;
					}else if(x-end > 30){
						begin = begin-backwardDistance;
					}
				}
			}	
		}
		interval[0] = begin+1;
		interval[1] = end+1;
		return interval;
	}
	
	public int firstYellow(double[][] data){
		int x=0;
		double yellowThresh = .02;
		for(int i = 1000; i< Integer.MAX_VALUE; i++){
			if(Double.valueOf(data[i][2] - data[i-5][3]) > yellowThresh){
				x=i;
				break;
			}
		}
		for(int k = 1; k<=10; k++){
			if(data[x-k][2] - data[x-k+1][2] > .0001){
				x = x-k+1;
				break;
			}
		}
		return x+1;
	}
	
	public int secondYellow(double[][] data){
		int x=0;
		double yellowThresh = .02;
		for(int i = 3200; i< Integer.MAX_VALUE; i++){
			if(Double.valueOf(data[i][2] - data[i-5][3]) > yellowThresh){
				x=i;
				break;
			}
		}
		for(int k = 1; k<=10; k++){
			if(data[x-k][2] - data[x-k+1][2] > .0001){
				x = x-k+1;
				break;
			}
		}
		return x+1;
	}
	
	
	public int countRows(String csv) throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(csv));
		int lines = 0;
		while (reader.readLine() != null) lines++;
		reader.close();
		return lines;
	}
}
